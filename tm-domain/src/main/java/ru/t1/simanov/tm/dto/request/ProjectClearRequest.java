package ru.t1.simanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class ProjectClearRequest extends AbstractUserRequest {

    public ProjectClearRequest(@Nullable String token) {
        super(token);
    }

}
