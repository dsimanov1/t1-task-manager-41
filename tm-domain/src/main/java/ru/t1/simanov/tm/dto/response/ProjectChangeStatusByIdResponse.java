package ru.t1.simanov.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ProjectChangeStatusByIdResponse extends AbstractResponse {
}
