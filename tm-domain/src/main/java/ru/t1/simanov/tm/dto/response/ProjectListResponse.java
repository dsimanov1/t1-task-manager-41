package ru.t1.simanov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.dto.model.ProjectDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<ProjectDTO> projects;

    public ProjectListResponse(@Nullable List<ProjectDTO> projects) {
        this.projects = projects;
    }

}
