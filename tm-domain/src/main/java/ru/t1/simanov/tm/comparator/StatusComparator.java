package ru.t1.simanov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.model.IHasStatus;

import java.util.Comparator;

public enum StatusComparator implements Comparator<IHasStatus> {

    INSTANCE;

    public int compare(
            @Nullable final IHasStatus o1,
            @Nullable final IHasStatus o2
    ) {
        if (o1 == null || o2 == null) return 0;
        o1.getStatus();
        o2.getStatus();
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
