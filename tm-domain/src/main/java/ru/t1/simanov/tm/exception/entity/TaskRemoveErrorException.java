package ru.t1.simanov.tm.exception.entity;

public final class TaskRemoveErrorException extends AbstractEntityNotFoundException {

    public TaskRemoveErrorException() {
        super("Error! Failed to delete task by id...");
    }

}
