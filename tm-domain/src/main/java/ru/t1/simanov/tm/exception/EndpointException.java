package ru.t1.simanov.tm.exception;

import org.jetbrains.annotations.NotNull;

public final class EndpointException extends AbstractException {

    public EndpointException(@NotNull String message) {
        super(message);
    }

}
