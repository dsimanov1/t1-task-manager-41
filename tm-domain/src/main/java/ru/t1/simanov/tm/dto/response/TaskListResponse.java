package ru.t1.simanov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractTaskResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListResponse(@Nullable List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}
