package ru.t1.simanov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(@Nullable final String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }

}
