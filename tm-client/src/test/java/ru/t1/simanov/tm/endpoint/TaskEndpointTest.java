package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.simanov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.simanov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.simanov.tm.api.endpoint.IUserEndpoint;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.enumerated.Status;
import ru.t1.simanov.tm.marker.IntegrationCategory;
import ru.t1.simanov.tm.dto.model.TaskDTO;
import ru.t1.simanov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.simanov.tm.constant.ProjectTestData.USER_PROJECT1_DESCRIPTION;
import static ru.t1.simanov.tm.constant.ProjectTestData.USER_PROJECT1_NAME;
import static ru.t1.simanov.tm.constant.TaskTestData.*;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_LOGIN;
import static ru.t1.simanov.tm.constant.UserTestData.USER_TEST_PASSWORD;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IUserEndpoint userEndpointClient = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private static final ITaskEndpoint taskEndpointClient = ITaskEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private String projectId1;

    @Nullable
    private String taskId1;

    private int taskIndex1;

    @Nullable
    private String taskId2;

    private int taskIndex2;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.loginUser(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        request.setPassword(USER_TEST_PASSWORD);
        userEndpointClient.registryUser(request);
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin(USER_TEST_LOGIN);
        userLoginRequest.setPassword(USER_TEST_PASSWORD);
        userToken = authEndpointClient.loginUser(userLoginRequest).getToken();
    }

    @AfterClass
    public static void tearDown() {
        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(USER_TEST_LOGIN);
        userEndpointClient.removeUser(request);
    }

    @Before
    public void before() {
        taskId1 = createTestTask(USER_TASK1_NAME, USER_TASK1_DESCRIPTION);
        taskIndex1 = 0;
        taskId2 = createTestTask(USER_TASK2_NAME, USER_TASK2_DESCRIPTION);
        taskIndex2 = 1;
        projectId1 = createTestProject(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);
    }

    @After
    public void after() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(taskEndpointClient.clearTask(request));
    }

    @NotNull
    private String createTestTask(final String name, final String description) {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(name);
        taskCreateRequest.setDescription(description);
        return taskEndpointClient.createTask(taskCreateRequest).getTask().getId();
    }

    @NotNull
    private String createTestProject(final String name, final String description) {
        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName(name);
        projectCreateRequest.setDescription(description);
        return projectEndpointClient.createProject(projectCreateRequest).getProject().getId();
    }

    @Nullable
    private TaskDTO findTestTaskById(final String id) {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setId(id);
        return taskEndpointClient.showByIdTask(request).getTask();
    }

    @Test
    public void changeStatusByIdTask() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIdRequest taskCreateRequestNullToken = new TaskChangeStatusByIdRequest(null);
        taskCreateRequestNullToken.setId(taskId1);
        taskCreateRequestNullToken.setStatus(status);
        Assert.assertThrows(Exception.class, () -> taskEndpointClient.changeStatusByIdTask(taskCreateRequestNullToken));
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setId(taskId1);
        request.setStatus(status);
        Assert.assertNotNull(taskEndpointClient.changeStatusByIdTask(request));
        @Nullable final TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void changeStatusByIndexTask() {
        @NotNull final Status status = Status.COMPLETED;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        request.setStatus(status);
        Assert.assertNotNull(taskEndpointClient.changeStatusByIndexTask(request));
        @Nullable final TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test
    public void clearTask() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(taskEndpointClient.clearTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNull(task);
        task = findTestTaskById(taskId2);
        Assert.assertNull(task);
    }

    @Test
    public void createTask() {
        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken);
        taskCreateRequest.setName(USER_TASK3_NAME);
        taskCreateRequest.setDescription(USER_TASK3_DESCRIPTION);
        @Nullable TaskDTO task = taskEndpointClient.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3_NAME, task.getName());
        Assert.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    @Test
    public void completeByIdTask() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken);
        request.setId(taskId1);
        Assert.assertNotNull(taskEndpointClient.completeByIdTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void completeByIndexTask() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        Assert.assertNotNull(taskEndpointClient.completeByIndexTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void listTask() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.listTask(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assert.assertNotNull(findTestTaskById(task.getId()));
        }
    }

    @Test
    public void removeByIdTask() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken);
        request.setId(taskId2);
        Assert.assertNotNull(taskEndpointClient.removeByIdTask(request));
        Assert.assertNull(findTestTaskById(taskId2));
    }

    @Test
    public void removeByIndexTask() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(userToken);
        request.setIndex(taskIndex2);
        Assert.assertNotNull(taskEndpointClient.removeByIndexTask(request));
        Assert.assertNull(findTestTaskById(taskId2));
    }

    @Test
    public void showByIdTask() {
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(userToken);
        request.setId(taskId1);
        @Nullable final TaskDTO task = taskEndpointClient.showByIdTask(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId1, task.getId());
    }

    @Test
    public void showByIndexTask() {
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        @Nullable final TaskDTO task = taskEndpointClient.showByIndexTask(request).getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskId1, task.getId());
    }

    @Test
    public void startByIdTask() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken);
        request.setId(taskId1);
        Assert.assertNotNull(taskEndpointClient.startByIdTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void startByIndexTask() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        Assert.assertNotNull(taskEndpointClient.startByIndexTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test
    public void updateByIdTask() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken);
        request.setId(taskId1);
        request.setName(USER_TASK3_NAME);
        request.setDescription(USER_TASK3_DESCRIPTION);
        Assert.assertNotNull(taskEndpointClient.updateByIdTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3_NAME, task.getName());
        Assert.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    @Test
    public void updateByIndexTask() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(userToken);
        request.setIndex(taskIndex1);
        request.setName(USER_TASK3_NAME);
        request.setDescription(USER_TASK3_DESCRIPTION);
        Assert.assertNotNull(taskEndpointClient.updateByIndexTask(request));
        @Nullable TaskDTO task = findTestTaskById(taskId1);
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK3_NAME, task.getName());
        Assert.assertEquals(USER_TASK3_DESCRIPTION, task.getDescription());
    }

    private void bindTaskProject(final String projectId, final String taskId) {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpointClient.bindToProjectTask(request);
    }

    @Test
    public void bindToProjectTask() {
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assert.assertNotNull(taskEndpointClient.bindToProjectTask(request));
        @NotNull final TaskShowByProjectIdRequest requestShow = new TaskShowByProjectIdRequest(userToken);
        requestShow.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.showByProjectIdTask(requestShow).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assert.assertEquals(taskId1, tasks.get(0).getId());
    }

    @Test
    public void showByProjectIdTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(userToken);
        request.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.showByProjectIdTask(request).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        for (TaskDTO task : tasks) {
            Assert.assertEquals(projectId1, task.getProjectId());
        }
    }

    @Test
    public void unbindFromProjectTask() {
        bindTaskProject(projectId1, taskId1);
        bindTaskProject(projectId1, taskId2);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(userToken);
        request.setProjectId(projectId1);
        request.setTaskId(taskId1);
        Assert.assertNotNull(taskEndpointClient.unbindFromProjectTask(request));
        @NotNull final TaskShowByProjectIdRequest requestShow = new TaskShowByProjectIdRequest(userToken);
        requestShow.setProjectId(projectId1);
        @Nullable final List<TaskDTO> tasks = taskEndpointClient.showByProjectIdTask(requestShow).getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(projectId1, tasks.get(0).getProjectId());
        Assert.assertEquals(taskId2, tasks.get(0).getId());
    }

}
