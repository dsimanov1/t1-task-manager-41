package ru.t1.simanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.request.ApplicationAboutRequest;
import ru.t1.simanov.tm.dto.response.ApplicationAboutResponse;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String NAME = "about";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final ApplicationAboutRequest request = new ApplicationAboutRequest();
        @NotNull final ApplicationAboutResponse response = getSystemEndpoint().getApplicationAbout(request);
        System.out.println("Name: " + response.getName());
        System.out.println("E-mail: " + response.getEmail());
    }

}
