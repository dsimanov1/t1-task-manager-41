package ru.t1.simanov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.command.AbstractCommand;

import java.util.Collection;

public final class CommandListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-cmd";

    @NotNull
    public static final String DESCRIPTION = "Show application commands.";

    @NotNull
    public static final String NAME = "commands";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        commands.forEach(m -> {
            @NotNull final String name = m.getName();
            if (!name.isEmpty()) System.out.println(name);
        });
    }

}
