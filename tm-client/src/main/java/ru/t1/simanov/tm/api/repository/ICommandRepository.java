package ru.t1.simanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(String argument);

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

    @Nullable
    AbstractCommand getCommandByName(String command);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}
