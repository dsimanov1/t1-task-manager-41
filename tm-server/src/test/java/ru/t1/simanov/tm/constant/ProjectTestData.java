package ru.t1.simanov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.simanov.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static ProjectDTO USER_PROJECT1 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_PROJECT2 = new ProjectDTO();

    @NotNull
    public final static ProjectDTO USER_PROJECT3 = new ProjectDTO();

    @NotNull
    public final static String NON_EXISTING_PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<ProjectDTO> USER_PROJECT_LIST = Arrays.asList(USER_PROJECT1, USER_PROJECT2, USER_PROJECT3);

    @NotNull
    public final static List<ProjectDTO> PROJECT_LIST = new ArrayList<>();

    static {
        USER_PROJECT_LIST.forEach(project -> project.setName("User Test Project " + project.getId()));
        USER_PROJECT_LIST.forEach(project -> project.setDescription("User Test Project " + project.getId() + " description"));
        PROJECT_LIST.add(USER_PROJECT1);
        PROJECT_LIST.add(USER_PROJECT2);
    }

}

