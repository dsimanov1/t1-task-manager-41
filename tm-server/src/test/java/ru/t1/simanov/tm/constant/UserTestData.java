package ru.t1.simanov.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.dto.model.UserDTO;
import ru.t1.simanov.tm.service.PropertyService;
import ru.t1.simanov.tm.util.HashUtil;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@UtilityClass
public final class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "USER_TEST_LOGIN";

    @NotNull
    public final static String USER_TEST_PASSWORD = "USER_TEST_PASSWORD";

    @NotNull
    public final static String USER_TEST_EMAIL = "USER_TEST@EMAIL";

    @NotNull
    public final static String ADMIN_TEST_LOGIN = "ADMIN_TEST_LOGIN";

    @NotNull
    public final static String ADMIN_TEST_PASSWORD = "ADMIN_TEST_PASSWORD";

    @NotNull
    public final static String ADMIN_TEST_EMAIL = "ADMIN_TEST@EMAIL";

    @NotNull
    public final static UserDTO USER_TEST = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN_TEST = new UserDTO();

    @Nullable
    public final static UserDTO NULL_USER = null;

    @NotNull
    public final static String NON_EXISTING_USER_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER_TEST, ADMIN_TEST);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    static {
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(HashUtil.salt(propertyService, USER_TEST_PASSWORD));
        USER_TEST.setEmail(USER_TEST_EMAIL);
        ADMIN_TEST.setLogin(ADMIN_TEST_LOGIN);
        ADMIN_TEST.setPasswordHash(HashUtil.salt(propertyService, ADMIN_TEST_PASSWORD));
        ADMIN_TEST.setEmail(ADMIN_TEST_EMAIL);
    }

}
