package ru.t1.simanov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.simanov.tm.api.repository.IUserRepository;
import ru.t1.simanov.tm.api.service.*;
import ru.t1.simanov.tm.enumerated.Role;
import ru.t1.simanov.tm.exception.entity.UserNotFoundException;
import ru.t1.simanov.tm.exception.field.*;
import ru.t1.simanov.tm.exception.user.ExistsEmailException;
import ru.t1.simanov.tm.exception.user.ExistsLoginException;
import ru.t1.simanov.tm.exception.user.RoleEmptyException;
import ru.t1.simanov.tm.dto.model.UserDTO;
import ru.t1.simanov.tm.util.HashUtil;

import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IConnectionService connectionService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @NotNull
    @Override
    public UserDTO add(@NotNull final UserDTO model) throws Exception {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return model;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return (userRepository.findOneById(id) != null);
        }
    }

    @Override
    public long getCount() throws Exception {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return (userRepository.getCount());
        }
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() throws Exception {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findAll();
        }
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneById(id);
        }
    }

    @Nullable
    @Override
    public UserDTO findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getCount()) throw new IndexIncorrectException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findOneByIndex(index);
        }
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        }
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return (userRepository.findByLogin(login) != null);
        }
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return (userRepository.findByEmail(email) != null);
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final UserDTO model) throws Exception {
        if (model == null) throw new UserNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.remove(model);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) throws Exception {
        @Nullable UserDTO user = findOneById(id);
        remove(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws Exception {
        @Nullable final UserDTO user = findByLogin(login);
        remove(user);
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.update(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
